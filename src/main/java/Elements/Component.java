package Elements;

import java.util.List;

public class Component {
    private String name;
    private String origin;
    private int price;
    private boolean peripheral;
    private boolean cooler;
    private String type;
    private List<String> ports;

    public Component() {

    }

    public Component(String name, String origin, int price, boolean peripheral, boolean cooler, String type, List<String> ports) {
        this.name = name;
        this.origin = origin;
        this.price = price;
        this.peripheral = peripheral;
        this.cooler = cooler;
        this.type = type;
        this.ports = ports;
        //   this.ports = ports;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isPeripheral() {
        return peripheral;
    }

    public void setPeripheral(boolean peripheral) {
        this.peripheral = peripheral;
    }

    public boolean isCooler() {
        return cooler;
    }

    public void setCooler(boolean cooler) {
        this.cooler = cooler;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Name : " + getName() + "\n" + "Origin : " + getOrigin() + "\n"
                + "Price " + getPrice() + "\n" + "Peripheral : " + isPeripheral() + "\n"
                + "Cooler : " + isCooler() + "\n" + "Type : " + getType() + "\n"
                + "Ports : " + getPorts()+"\n";

    }

    void addPort() {

    }

    public List<String> getPorts() {
        return ports;
    }

    public void setPorts(List<String> ports) {
        this.ports = ports;
    }
}
