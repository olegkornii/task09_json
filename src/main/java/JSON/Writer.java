package JSON;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Writer {
    public static void write(){
        final String json = ("/home/oleg/home/oleg/Документи/IdeaProjects/EpamJSONTask/src/main/resources/componentJSON.json");

        try {
            Map<String,Object> map =  new HashMap<>();
            String[] arr = {"TPC","TPB","USB"};
            File f = new File(json);
            ObjectMapper objectMapper = new ObjectMapper();

            JSONObject jsonObject = new JSONObject();

            map.put("name","AMD 11");
            map.put("origin","USA");
            map.put("price",1000);
            map.put("peripheral",true);
            map.put("cooler",false);
            map.put("type","CPU");
            map.put("ports",arr);
            jsonObject.putAll(map);
            System.out.println(jsonObject);
            objectMapper.writeValue(f,jsonObject);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        write();
    }
}
