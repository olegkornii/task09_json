package JSON;

import Elements.Component;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;

public class Reader {
    File jsonSchema = new File("/home/oleg/home/oleg/Документи/IdeaProjects/EpamJSONTask/src/main/resources/componentJSON.json");
    static ObjectMapper objectMapper = new ObjectMapper();
    static File json = new File("/home/oleg/home/oleg/Документи/IdeaProjects/EpamJSONTask/src/main/resources/componentJSON.json");

    public static void getComponents() {
        Component[] components = new Component[0];

        try {

            components = objectMapper.readValue(json, Component[].class);
            for (Component c :
                    components) {
                c.setPrice(c.getPrice()-1);
                System.out.println(c);
            }
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        getComponents();
    }
}


